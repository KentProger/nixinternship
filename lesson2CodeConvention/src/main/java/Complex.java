
/**
* class Complex
*
* Предназначен для создания комплексных чисел
*
* @since 1.0
* @author Igor Dragunov
* */
public class Complex {
    /**
    * Имя переменной должно отображать ее смысл
    *   Старый код
    *   private final double re;
    *   private final double im;
    * */
    private final double realPart;
    private final double imaginaryPart;

    public Complex(double realPart, double imaginaryPart) {
        if (Double.isNaN(realPart) || Double.isNaN(imaginaryPart)) {
            throw new ArithmeticException();
        }
        this.realPart = realPart;
        this.imaginaryPart = imaginaryPart;
    }

    /**
    *   Имена методов должны быть глаголами и должны показывать что метод делает
    * также здесь был слишком большой отступ от сигнатуры к телу метода ,в первом методе
    * Все блоки кода должны оборачиваться фигурными скобками. открывающая скобка должна
    * находится в конце строки на которой находится инструкция требующая блока кода,
    * это также относиться и к методам
    * Старый код :
    * public double realPart() { return realPart; }
    * public double imaginaryPart() { return imaginaryPart; }
    *
    * */
    public double getRealPart() {

        return realPart;
    }
    public double getImaginaryPart() {

        return imaginaryPart;
    }

    public Complex add(Complex c) {
        return new Complex(realPart + c.realPart, imaginaryPart + c.imaginaryPart);
    }

    /* Для удобной читаемости, лучше ,чтобы анотация и метод были на разных строчках */
    @Override
    public boolean equals(Object o) {
        if (o == this)
            return true;
        if (!(o instanceof Complex))
            return false;
        Complex c = (Complex) o;
        /*Здесь нужно после переноса отступить 8 пробелов */
        return Double.compare(realPart, c.realPart) == 0
                       && Double.compare(imaginaryPart, c.imaginaryPart) == 0;
    }

    @Override
    public int hashCode() {
        int result = 17 + Double.hashCode(realPart);
        result = 31 * result + Double.hashCode(imaginaryPart);
        return result;
    }

    @Override
    public String toString() {
        return "(" + realPart + (imaginaryPart < 0 ? "" : "+") + imaginaryPart + "i)";
    }


    public static void main(String[] args) {
        /* здесь также имя переменной должно быть более понятное,
        *  с Complex z на Complex complex
        * */
        Complex complex = new Complex(1, 2);
        System.out.println(complex.add(complex));
    }
}